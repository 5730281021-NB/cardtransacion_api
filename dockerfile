FROM openjdk:11
RUN apt-get update && apt-get install -y vim && apt-get install -y apt-utils &&  apt-get -y install sudo
RUN  curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
RUN  sudo apt install nodejs

RUN sudo apt-get install -y build-essential

RUN apt-get install  -y python

RUN mkdir -p /cardtransaction   
WORKDIR /cardtransaction                        

COPY . .

RUN npm install --save

EXPOSE 8203

ENTRYPOINT ["/bin/sh","-c"]

CMD ["npm start"] 

